<?php 
/* Template Name: Investor Page */ 
get_header();

$page_id = 8;  //Page ID
$page_data = get_page( $page_id ); 

//store page title and content in variables
$title = $page_data->post_title; 
$content = apply_filters('the_content', $page_data->post_content);
?>

    <section class="subsidiaries">

        <div class="container">
          <div class="row justify-content-center">

          <div class="col-lg-9 col-sm-6 text-center" style="z-index: 1">
<h2 class="pb-3">SIMSI INVESTMENTS PROPDEV-CROWD</h2>
              <p>Simsi Investments is introducing a private real estate/private investment platform called PROPDEV- CROWD (PROPERTY DEVELOPMENT – CROWDFUNDING). In order to be part of our Crowd Funding opportunity register below:</p>
          </div>
          </div>  
        </div> 
          <div class="overlay"></div> 
      </section> 

<section class="pt-5 pb-5">
      <div class="container">
          <div class="row">
           <div class="col-lg-6 col-sm-6">
           <div class="simsis"> 
                <h2>Crowd Funding Registration</h2>
                 <iframe width="100%" height="100%" scrolling="auto" src="https://www-simsi-group-com.filesusr.com/html/250b39_cdd60b0bcc11e07ad7b3f3d202652b05.html"></iframe>
             </div>
            </div>

          <div class="col-lg-6 col-sm-6">
           <div class="simsis"> 
                <h2>Crowd Funding Login </h2>
                <p>In order to update your information, login to your crowd funding account below:</p>
                 <iframe width="100%" height="100%" scrolling="auto" src="https://www-simsi-group-com.filesusr.com/html/250b39_ba7416355e07dc24b4d42a3bfc150277.html"></iframe>
             </div>
            </div> 

          </div>
      </div> 
</section>
    
<?php
get_footer();


?>