<?php

function register_my_menu() {
register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
if( in_array('current-menu-item', $classes) ){
$classes[] = 'active nav-item';
}
return $classes;
}

// let's add our custom class to the actual link tag    

function atg_menu_classes($classes, $item, $args) {
if($args->theme_location == 'topnav') {
$classes[] = 'nav-link';
}
return $classes;
}
add_filter('nav_menu_css_class', 'atg_menu_classes', 1, 3);

function add_menuclass($ulclass) {
return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
}
add_filter('wp_nav_menu','add_menuclass');


/*
* Creating a function to create our CPT
*/
function register_custom_user_column($columns) {
    $columns['status'] = 'Status';
    return $columns;
}

function custom_post_type2() {

// Set UI labels for Custom Post Type
$labels = array(
'name'                => _x( 'Simsi Approach', 'Post Type General Name', 'twentythirteen' ),
'singular_name'       => _x( 'Simsi Approach', 'Post Type Singular Name', 'twentythirteen' ),
'menu_name'           => __( 'Simsi Approach', 'twentythirteen' ),
'parent_item_colon'   => __( 'Parent Simsi Approach', 'twentythirteen' ),
'all_items'           => __( 'All Simsi Approach', 'twentythirteen' ),
'view_item'           => __( 'View Simsi Approach', 'twentythirteen' ),
'add_new_item'        => __( 'Add New Simsi Approach', 'twentythirteen' ),
'add_new'             => __( 'Add New', 'twentythirteen' ),
'edit_item'           => __( 'Edit Simsi Approach', 'twentythirteen' ),
'update_item'         => __( 'Update Simsi Approach', 'twentythirteen' ),
'search_items'        => __( 'Search Simsi Approach', 'twentythirteen' ),
'not_found'           => __( 'Not Found', 'twentythirteen' ),
'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
);

// Set other options for Custom Post Type

$args = array(
'label'               => __( 'Simsi Approach', 'twentythirteen' ),
'description'         => __( 'Simsi Approach news and reviews', 'twentythirteen' ),
'labels'              => $labels,
// Features this CPT supports in Post Editor
'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
// You can associate this CPT with a taxonomy or custom taxonomy. 
'taxonomies'          => array( 'genres' ),
/* A hierarchical CPT is like Pages and can have
* Parent and child items. A non-hierarchical CPT
* is like Posts.
*/ 
'hierarchical'        => false,
'public'              => true,
'show_ui'             => true,
'show_in_menu'        => true,
'show_in_nav_menus'   => true,
'show_in_admin_bar'   => true,
'menu_position'       => 5,
'can_export'          => true,
'has_archive'         => true,
'exclude_from_search' => false,
'publicly_queryable'  => true,
'capability_type'     => 'page',
);

// Registering your Custom Post Type
register_post_type( 'approach', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/

add_action( 'init', 'custom_post_type2', 0 );


function custom_post_type() {

// Set UI labels for Custom Post Type
$labels = array(
'name'                => _x( 'Services', 'Post Type General Name', 'twentythirteen' ),
'singular_name'       => _x( 'Services', 'Post Type Singular Name', 'twentythirteen' ),
'menu_name'           => __( 'Services', 'twentythirteen' ),
'parent_item_colon'   => __( 'Parent Services', 'twentythirteen' ),
'all_items'           => __( 'All Services', 'twentythirteen' ),
'view_item'           => __( 'View Services', 'twentythirteen' ),
'add_new_item'        => __( 'Add New Services', 'twentythirteen' ),
'add_new'             => __( 'Add New', 'twentythirteen' ),
'edit_item'           => __( 'Edit Services', 'twentythirteen' ),
'update_item'         => __( 'Update Services', 'twentythirteen' ),
'search_items'        => __( 'Search Services', 'twentythirteen' ),
'not_found'           => __( 'Not Found', 'twentythirteen' ),
'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
);

// Set other options for Custom Post Type

$args = array(
'label'               => __( 'Services', 'twentythirteen' ),
'description'         => __( 'Services news and reviews', 'twentythirteen' ),
'labels'              => $labels,
// Features this CPT supports in Post Editor
'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
// You can associate this CPT with a taxonomy or custom taxonomy. 
'taxonomies'          => array( 'genres' ),
/* A hierarchical CPT is like Pages and can have
* Parent and child items. A non-hierarchical CPT
* is like Posts.
*/ 
'hierarchical'        => false,
'public'              => true,
'show_ui'             => true,
'show_in_menu'        => true,
'show_in_nav_menus'   => true,
'show_in_admin_bar'   => true,
'menu_position'       => 5,
'can_export'          => true,
'has_archive'         => true,
'exclude_from_search' => false,
'publicly_queryable'  => true,
'capability_type'     => 'page',
);

// Registering your Custom Post Type
register_post_type( 'services', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/

add_action( 'init', 'custom_post_type', 0 );



add_theme_support('post-thumbnails', array(
'post',
'page',
'services',
'approach',
));
$result = add_role(
'vendor',
__( 'Vendor' ),
array(
'read'         => true,  // true allows this capability

)
);
function theme_show_user_zip_code_data( $value, $column_name, $user_id ) {

if( 'zip_code' == $column_name ) {
return get_user_meta( $user_id, 'zip_code', true );
} // end if

} // end theme_show_user_zip_code_data
add_action( 'manage_users_custom_column', 'theme_show_user_zip_code_data', 10, 3 );
?>
<?php
/**
* Create A Simple Theme Options Panel
*
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
exit;
}

// Start Class
if ( ! class_exists( 'WPEX_Theme_Options' ) ) {

class WPEX_Theme_Options {

/**
* Start things up
*
* @since 1.0.0
*/
public function __construct() {

// We only need to register the admin panel on the back-end
if ( is_admin() ) {
add_action( 'admin_menu', array( 'WPEX_Theme_Options', 'add_admin_menu' ) );
add_action( 'admin_init', array( 'WPEX_Theme_Options', 'register_settings' ) );
}

}

/**
* Returns all theme options
*
* @since 1.0.0
*/
public static function get_theme_options() {
return get_option( 'theme_options' );
}

/**
* Returns single theme option
*
* @since 1.0.0
*/
public static function get_theme_option( $id ) {
$options = self::get_theme_options();
if ( isset( $options[$id] ) ) {
return $options[$id];
}
}

/**
* Add sub menu page
*
* @since 1.0.0
*/
public static function add_admin_menu() {
add_menu_page(
esc_html__( 'Theme Settings', 'text-domain' ),
esc_html__( 'Theme Settings', 'text-domain' ),
'manage_options',
'theme-settings',
array( 'WPEX_Theme_Options', 'create_admin_page' )
);
}

/**
* Register a setting and its sanitization callback.
*
* We are only registering 1 setting so we can store all options in a single option as
* an array. You could, however, register a new setting for each option
*
* @since 1.0.0
*/
public static function register_settings() {
register_setting( 'theme_options', 'theme_options', array( 'WPEX_Theme_Options', 'sanitize' ) );
}

/**
* Sanitization callback
*
* @since 1.0.0
*/
public static function sanitize( $options ) {

// If we have options lets sanitize them
if ( $options ) {

// Checkbox
if ( ! empty( $options['checkbox_example'] ) ) {
$options['checkbox_example'] = 'on';
} else {
unset( $options['checkbox_example'] ); // Remove from options if not checked
}

// Input
if ( ! empty( $options['input_example'] ) ) {
$options['input_example'] = sanitize_text_field( $options['input_example'] );
} else {
unset( $options['input_example'] ); // Remove from options if empty
}

// Select
if ( ! empty( $options['select_example'] ) ) {
$options['select_example'] = sanitize_text_field( $options['select_example'] );
}

}

// Return sanitized options
return $options;

}

/**
* Settings page output
*
* @since 1.0.0
*/
public static function create_admin_page() { ?>

<div class="wrap">

<h1><?php esc_html_e( 'Theme Options', 'text-domain' ); ?></h1>

<form method="post" action="options.php">

<?php settings_fields( 'theme_options' ); ?>

<table class="form-table wpex-custom-admin-login-table">

<?php // Checkbox example ?>
<tr valign="top">
<th scope="row"><?php esc_html_e( 'Email Address', 'text-domain' ); ?></th>
<td>
<?php $value = self::get_theme_option( 'email_address' ); ?>
<input type="text" name="theme_options[email_address]" value="<?php echo esc_attr( $value ); ?>">
</td>
</tr>

<?php // Text input example ?>
<tr valign="top">
<th scope="row"><?php esc_html_e( 'Phone No.', 'text-domain' ); ?></th>
<td>
<?php $value = self::get_theme_option( 'phone' ); ?>
<input type="text" name="theme_options[phone]" value="<?php echo esc_attr( $value ); ?>">
</td>
</tr>
<tr valign="top">
<th scope="row"><?php esc_html_e( 'Address', 'text-domain' ); ?></th>
<td>
<?php $value = self::get_theme_option( 'address' ); ?>
<textarea rows="5" cols="30" name="theme_options[address]" value=""><?php echo esc_attr( $value ); ?></textarea>
</td>
</tr>
<tr valign="top">
<th scope="row"><?php esc_html_e('Facebook', 'text-domain' ); ?></th>
<td>
<?php $value = self::get_theme_option( 'facebook' ); ?>
<input type="text" name="theme_options[facebook]" value="<?php echo esc_attr( $value ); ?>">
</td>
</tr>
<tr valign="top">
<th scope="row"><?php esc_html_e( 'Twitter', 'text-domain' ); ?></th>
<td>
<?php $value = self::get_theme_option( 'twitter' ); ?>
<input type="text" name="theme_options[twitter]" value="<?php echo esc_attr( $value ); ?>">
</td>
</tr>
<tr valign="top">
<th scope="row"><?php esc_html_e( 'Linkedin', 'text-domain' ); ?></th>
<td>
<?php $value = self::get_theme_option( 'linkedin' ); ?>
<input type="text" name="theme_options[linkedin]" value="<?php echo esc_attr( $value ); ?>">
</td>
</tr>

<?php // Select example ?>


</table>

<?php submit_button(); ?>

</form>

</div><!-- .wrap -->
<?php }

}
}
new WPEX_Theme_Options();

// Helper function to use in your theme to return a theme option value
function myprefix_get_theme_option( $id = '' ) {
return WPEX_Theme_Options::get_theme_option( $id );
}

