<?php 
/* Template Name: About Page */ 
get_header();

$page_id = 8;  //Page ID
$page_data = get_page( $page_id ); 

//store page title and content in variables
$title = $page_data->post_title; 
$content = apply_filters('the_content', $page_data->post_content);
?>

       <section class="about">

        <div class="container">
          <div class="row">

          <div class="col-lg-12 col-sm-6 text-center" style="z-index: 1"><h1>About us</h1></div>
          </div>  
        </div> 
          <div class="overlay"></div> 
      </section>  

        
 
      <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-10 col-sm-6 pt-5 pb-5 text-center">
           <?php echo $content = apply_filters('the_content', $page_data->post_content); ?>
            </div>

<?php 

$posts = get_field('services_section');

if( $posts ): ?>

    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <?php  $feat_image_url = wp_get_attachment_url( get_post_thumbnail_id() ); ?>

            <div class="col-lg-4 col-sm-6 portfolio-item investments">
          <div class="card text-center">
             <div class="pt-3 pl-5 pr-5"><img class="card-img-top img-fluid" src="<?php echo $feat_image_url; ?>" alt=""></div> 
            <div class="card-body">
              <h4 class="card-title">
                <?php the_title(); ?>
              </h4>
              

               
            </div>
          </div>
        </div>
   <?php endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>
        
 

        <div class="col-lg-10 col-sm-6 pt-2 pb-5 text-center">
                             <p><?php the_field('services_content'); ?></p>

            </div>
          </div>
      </div> 

     <section class="visionand">

        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-5 col-sm-6 text-center">
              <div class="vision">
                <h2>Vision</h2>
                <p><?php the_field('vision'); ?></p>
               </div> 
            </div>

            <div class="col-lg-5 col-sm-6 text-center">
              <div class="vision">
                <h2>Mission</h2>
                <p><?php the_field('mission'); ?></p>
               </div> 
            </div>  

          </div>
        </div>
     </section>           


<div class="container">

      <div class="row justify-content-center">
        <div class="col-lg-8  text-center">
          <h1 class="pt-5">Simsi Approach</h1>
          <hr class="header-hr">
   <?php the_field('simsi_approach'); ?>
        </div>
<?php 
$i=1;
$query = new WP_Query( array( 'post_type' => 'approach', 'paged' => $paged ) );

if ( $query->have_posts() ) : ?>
<?php while ( $query->have_posts() ) : $query->the_post(); ?>

<?php  $feat_image_url = wp_get_attachment_url( get_post_thumbnail_id() ); ?>

        <div class="col-lg-6 col-sm-6 <?php if($i==3) { echo 'pt-3';} ?>">
          <div class="border">
          <img src="<?php echo $feat_image_url; ?>" alt="" class="img-fluid">
          <h3 class="pt-3"><?php the_title(); ?></h3>
          <p><?php the_content(); ?></p>
        </div>
        </div>
<?php $i++; endwhile; wp_reset_postdata(); ?>
<!-- show pagination here -->
<?php else : ?>
<!-- show 404 error here -->
<?php endif; ?>
        
   
      </div>
      <!-- /.row -->
    </div>
<?php
get_footer();


?>