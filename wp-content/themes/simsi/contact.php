<?php 
/* Template Name: Contact Page */ 
get_header();

$page_id = 8;  //Page ID
$page_data = get_page( $page_id ); 

//store page title and content in variables
$title = $page_data->post_title; 
$content = apply_filters('the_content', $page_data->post_content);
?>
<style type="text/css">
  .screen-reader-response{
    display: none;
  }
span.wpcf7-not-valid-tip {
    color: red;
}
.wpcf7-response-output{
  font-size: 18px;
    color: red;
}
</style>
    <section class="subsidiaries">

        <div class="container">
          <div class="row justify-content-center">

          <div class="col-lg-9 col-sm-6 text-center" style="z-index: 1">
<h1>Contact Us</h1>
          </div>
          </div>  
        </div> 
          <div class="overlay"></div> 
      </section> 


<section>
<iframe data-src="https://static.parastorage.com/services/santa/1.3690.27/static/external/googleMap.html?language=en" width="100%" height="100%" frameBorder="0" scrolling="no" title="Google Maps" aria-label="Google Maps"></iframe>

</section>


    <div class="container">
        <div class="row">
          <div class="col-lg-6 col-sm-6">
        <?php echo do_shortcode('[contact-form-7 id="160" title="my form"]'); ?>
          </div>

          <div class="col-lg-6 col-sm-6">
<address>
  <strong>Twitter, Inc.</strong><br>
 <?php echo $value = myprefix_get_theme_option( 'address' ); ?><br>
  <abbr title="Phone">Phone:</abbr> <?php echo $value = myprefix_get_theme_option( 'phone' ); ?>
</address>

<address>
  <strong>Email</strong><br>
  <a href="mailto:<?php echo $value = myprefix_get_theme_option( 'email_address' ); ?>"><?php echo $value = myprefix_get_theme_option( 'email_address' ); ?></a>
</address>
          </div>
      </div>        
    </div>

    
<?php
get_footer();


?>