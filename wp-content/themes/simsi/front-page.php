<?php 
/* Template Name: Home Page */ 

get_header();
session_start();

if($_COOKIE['user']){
              update_user_meta( $_COOKIE['user'], 'status','Active' );

}

$page_id = 6;  //Page ID
$page_data = get_page( $page_id ); 

//store page title and content in variables
$title = $page_data->post_title; 
$content = apply_filters('the_content', $page_data->post_content);
?>

      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <!-- <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol> -->
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->

          <div class="carousel-item active">
            <div class=" d-none d-md-block">
             <img src="<?php echo the_field('home_banner') ?>" class="img-fluid" alt="">
            </div>
          </div>
          
         
        </div>
       <!--  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a> -->
      </div>
  

    <!-- Page Content -->
    <div class="container">

      <div class="row">
      	<div class="col-lg-12 text-center">
          <h1 class="pt-5">Welcome to Simsi Group</h1>
          <hr class="header-hr">
      		<p class="pt-4 pb-4"><?php echo $content; ?></p>
      	</div>
       <?php 
$query = new WP_Query( array( 'post_type' => 'services', 'paged' => $paged ) );

if ( $query->have_posts() ) : ?>
<?php while ( $query->have_posts() ) : $query->the_post(); ?>

<?php  $feat_image_url = wp_get_attachment_url( get_post_thumbnail_id() ); ?>

         <div class="col-lg-4 col-sm-6 portfolio-item investments">
          <div class="card text-center">
             <div class="pt-3 pl-5 pr-5"><img class="card-img-top img-fluid" src="<?php echo $feat_image_url; ?>" alt=""></div> 
            <div class="card-body">
              <h4 class="card-title">
                <?php the_title(); ?>
              </h4>
              <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

              <a href="#">Read More</a>
            </div>
          </div>
        </div>
<?php endwhile; wp_reset_postdata(); ?>
<!-- show pagination here -->
<?php else : ?>
<!-- show 404 error here -->
<?php endif; ?>
     
       
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->

  <section class="register">

  	<div class="container">

      <div class="row row justify-content-center">
      	<div class="col-lg-8 text-center">
      		<h2 class="pb-3"><?php echo the_field('registration_title'); ?></h2>

      		<?php echo the_field('registration_content'); ?>
<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-warning pl-5 pr-5 mt-4">Register</button>
 
 
      	</div>	
      </div>		

    </div>  		

  </section>
<?php
get_footer();


?>