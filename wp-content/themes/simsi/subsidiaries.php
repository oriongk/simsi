<?php 
/* Template Name: Subsidiaries Page */ 
get_header();

$page_id = 10;  //Page ID
$page_data = get_page( $page_id ); 

//store page title and content in variables
$title = $page_data->post_title; 
$content = apply_filters('the_content', $page_data->post_content);
?>

      <section class="subsidiaries">

        <div class="container">
          <div class="row">

          <div class="col-lg-12 col-sm-6 text-center" style="z-index: 1"><h1>Subsidiaries</h1></div>
          </div>  
        </div> 
          <div class="overlay"></div> 
      </section>  

      <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-12 col-sm-6 pt-5 pb-2">
              <p><?php echo $content; ?></p>
            </div>

 

        <div class="col-lg-6 col-sm-6 pt-2 pb-5">

         
        <?php echo the_field('simsi_consulting'); ?>

             </div>

            <div class="col-lg-6 col-sm-6 pt-2 pb-5"><img src="<?php echo the_field('image') ?>" class="img-fluid"></div>
 <div class="col-lg-6 col-sm-6 pt-2 pb-5"><img src="<?php echo the_field('image-1') ?>" class="img-fluid"></div>
<div class="col-lg-6 col-sm-6 pt-2 pb-5">

        <?php echo the_field('consulting_–_core_servicesfunctions'); ?>
</div>
          </div>
      </div> 

     <section class="" style="background:url(<?php bloginfo('stylesheet_directory'); ?>/assets/images/Infrastructure.jpg) no-repeat center; background-size:cover;background-attachment: fixed;">
<div class="overlay1"> 
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-10 col-sm-6">
           
                <h2><?php echo the_field('title-3'); ?></h2>
                <?php echo the_field('contant-3'); ?>
            </div>

             

          </div>
        </div>
</div>
     </section> 

     <section class="visionand" style="background-color: transparent; color: #333">

        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-sm-6">
           <div class="simsis"> 
                <h2><?php echo the_field('estate_title'); ?></h2>
<?php echo the_field('estate_content'); ?>
             </div>
            </div>

<div class="col-lg-6 col-sm-6">
           <div class="simsis"> 
                <h2><?php echo the_field('investments_title'); ?></h2>
                <?php echo the_field('investments_content'); ?>
             </div>
            </div>
             

          </div>
        </div>
     </section>    
    
<?php
get_footer();


?>