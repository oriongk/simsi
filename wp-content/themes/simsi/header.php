<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> </title>

    <!-- Bootstrap core CSS -->
    <link href="<?php bloginfo('stylesheet_directory'); ?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php bloginfo('stylesheet_directory'); ?>/assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700" rel="stylesheet">
<style type="text/css">
  .menu-header-menu-container{ margin: auto; }
</style>
<script>
      var span = document.getElementsByTagName('span')[0];
      span.textContent = 'interactive'; // change DOM text content
      span.style.display = 'inline';  // change CSSOM property
      // create a new element, style it, and append it to the DOM
      var loadTime = document.createElement('div');
      loadTime.textContent = 'You loaded this page on: ' + new Date();
      loadTime.style.color = 'blue';
      document.body.appendChild(loadTime);
    </script>
  </head>

  <body>
<header>
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-sm-6"><i class="fas fa-envelope"></i> Email-Id : simsi-IT@simsi.co.za</div>
          <div class="col-lg-4 col-sm-6 text-center">Welcome Visitor</div>
          <div class="col-lg-4 col-sm-6 text-right"><i class="fas fa-phone"></i> Phone Number : (031) 569-5058</div>
        </div>
      </div>  
    </header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light ">
      <div class="container">

        <a class="navbar-brand" href="javascript:"><img class="img-fluid" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/logo.png" alt="logo" /></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">

         <?php
wp_nav_menu( array( 
    'theme_location' => 'header-menu', 
    'menu_class' => 'navbar-nav ml-auto' ) ); 
?>
        </div>
      </div>
    </nav>
