<?php 
/* Template Name: Supplier Page */ 
get_header();

$page_id = 8;  //Page ID
$page_data = get_page( $page_id ); 

//store page title and content in variables
$title = $page_data->post_title; 
$content = apply_filters('the_content', $page_data->post_content);
?>

      <section class="subsidiaries">

        <div class="container">
          <div class="row">

          <div class="col-lg-12 col-sm-6 text-center" style="z-index: 1"><h1>Supplier Database</h1></div>
          </div>  
        </div> 
          <div class="overlay"></div> 
      </section>  

      <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-10 col-sm-6 pt-5 pb-2 text-center">
              <h2 class="pb-3"><?php echo the_field('registration_title'); ?></h2>
           <?php echo the_field('registration_content'); ?>

 <button type="button"  data-toggle="modal" data-target="#myModal" class="btn btn-warning pl-5 pr-5 mt-4">Register</button>
 <br/><br/>
  <br/><br/>
 <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/mv2.png" class="img-fluid">
            </div>
 

          </div>
      </div> 

     <section class=" pt-5" style=" background-color: #fff">
 
        <div class="container">
          <div class="row">
<div class="col-lg-12 text-center pt-5 pb-4">
            <h2>Minimum Terms and Conditions</h2>
          </div>
    
           
                <?php
$X=1;
$X1=1;
// check if the repeater field has rows of data
if( have_rows('terms_and_conditions') ):

  // loop through the rows of data
    while ( have_rows('terms_and_conditions') ) : the_row();
        if($X1==1) { ?> <div class="col-lg-6 col-sm-6"> <?php } 
        if($X>=8) { $X=0; ?> <div class="col-lg-6 col-sm-6"> <?php } ?>
       
        <strong> <?php the_sub_field('title'); ?></strong>
        <p><?php the_sub_field('content'); ?></p>
        
        <?php if($X==7) { ?> </div> <?php } ?>
        <?php if($X1>=14) { $X1=0; ?> </div> <?php } ?>
  <?php
  $X++; $X1++;
    endwhile;

else :

    // no rows found

endif;

?>
                 
              
                
                

                
    

        
           </div>
        </div>
     </section> 
    
<?php
get_footer();


?>