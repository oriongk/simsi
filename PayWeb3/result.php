<?php
	/*
	 * Once the client has completed the transaction on the PayWeb page, they will be redirected to the RETURN_URL set in the initate
	 * Here we will check the transaction status and process accordingly
	 *
	 */

	/*
	 * Sessions used here only because we can't get the PayGate ID, Transaction reference and secret key on the result page.
	 */
    session_name('paygate_payweb3_testing_sample');
    session_start();

	include_once('../payget/PayWeb3/lib/php/global.inc.php');

	/*
	 * Include the helper PayWeb 3 class
	 */
	require_once('../payget/PayWeb3/paygate.payweb3.php');

	/*
	 * insert the returned data as well as the merchant specific data PAYGATE_ID and REFERENCE in array
	 */
	$data = array(
		'PAYGATE_ID'         => $_SESSION['pgid'],
		'PAY_REQUEST_ID'     => $_POST['PAY_REQUEST_ID'],
		'TRANSACTION_STATUS' => $_POST['TRANSACTION_STATUS'],
		'REFERENCE'          => $_SESSION['reference'],
		'CHECKSUM'           => $_POST['CHECKSUM']
	);



	/*
	 * initiate the PayWeb 3 helper class
	 */
	$PayWeb3 = new PayGate_PayWeb3();
	/*
	 * Set the encryption key of your PayGate PayWeb3 configuration
	 */
	$PayWeb3->setEncryptionKey($_SESSION['key']);
	/*
	 * Check that the checksum returned matches the checksum we generate
	 */
	$isValid = $PayWeb3->validateChecksum($data)
	

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	    <meta http-equiv="content-type" content="text/html; charset=utf-8">
	    <title>PayWeb 3 - Result</title>
		<link rel="stylesheet" href="../lib/css/bootstrap.min.css">
		<link rel="stylesheet" href="../lib/css/core.css">
	</head>
	<body>
		<div class="container-fluid" style="min-width: 320px;">
		
<style type="text/css">
	
.make_payement
{ 
	border: 2px solid grey;
	background-color:#fff;
	margin: 0 auto;
	width:45%;
	text-align: left;
}
.pay_head
		{
	
		text-align: left;
		color: blue;
		
		}
 .text
  {
	 margin-left:15px;
	font-size: 20px;
	color: midnightblue;
	font-style: italic;
	color: # 333;
	margin-top:15px;

  }
  .amount
  {
  	    padding-left: 30%;
    color: black;
  }
  .paypal
  {
	    padding-left: 18%;
	color: black;
  }
  .suess
  {
	    padding-left: 25%;
	color: black;
  }
  .ch_id
  {
  	    padding-left:29%;
    color: black;
  }
  .tra_id
  {
  	    padding-left: 22%;
    color: black;
  }
  .date
  {
  	    padding-left: 31%;
    color: black;
  }
  .s1
  {
	padding-left:10px;
	color: green;
  }
  .pt
  {
  	    padding-left: 15px;
  }

</style>




<div class="make_payement">
    <h1 class="pay_head">
      
  <span class="pt">Payment</span><span class="s1">Sussessfully </span></br>
         
    </h1>
<div class="text">
  <p class="text1">Amount <span class="amount">1zer</span></p>
  <p class="text1">Payment Method<span class="Paypal">Paypal</span></p>  
<p class="text1">Transaction Id<span class="tra_id"><?php echo $data['PAY_REQUEST_ID']; ?></span></p>
  <p class="text1">Date<span class="date"><?php echo date('Y-m-d'); ?></span></p>
  </div>

</div>

		<script type="text/javascript" src="../lib/js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="../lib/js/bootstrap.min.js"></script>
	</body>
</html>

